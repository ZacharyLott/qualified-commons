package com.qualifiedigital.commons.core.servlets;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.AssetReferenceSearch;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

@Component(service = Servlet.class, name = "Push to production Servlet",
        property = {
                "sling.servlet.paths=/bin/pushtoprod",
                "sling.servlet.methods=post"
        }
)
public class PushToProdReplicationServlet extends SlingAllMethodsServlet {

    protected static final Logger log = LoggerFactory.getLogger(PushToProdReplicationServlet.class);

    @Reference
    private Replicator replicator;

    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        log.info("----------< Processing starts >----------");
        try {
            String path = request.getParameter("path");
            String contentpath = request.getParameter("contentPath");
            if(path == null && contentpath != null) {
                path = contentpath;
            }
            else if(path == null && contentpath == null) {
                log.info("Path is null, aborting push.");
                return;
            }
            ResourceResolver resolver = request.getResourceResolver();
            Session session = resolver.adaptTo(Session.class);
            session.refresh(true);
            replicateContent(session, path);
            activatePageAssets(resolver, path);
            session.save();
            log.info("----------< Processing ends >----------");
            response.getWriter().println("Pushed content");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void activatePageAssets(ResourceResolver resolver, String path) {
        Set<String> pageAssetPaths = getPageAssetsPaths(resolver, path);
        if (pageAssetPaths == null) {
            return;
        }
        Session session = resolver.adaptTo(Session.class);
        for (String assetPath : pageAssetPaths) {
            replicateContent(session, assetPath);
        }
    }

    private Set<String> getPageAssetsPaths(ResourceResolver resolver, String pagePath) {
        PageManager pageManager = resolver.adaptTo(PageManager.class);
        Page page = pageManager.getPage(pagePath);
        if (page == null) {
            return new LinkedHashSet<>();
        }
        Resource resource = page.getContentResource();
        AssetReferenceSearch assetReferenceSearch = new AssetReferenceSearch(resource.adaptTo(Node.class),
                DamConstants.MOUNTPOINT_ASSETS, resolver);
        Map<String, Asset> assetMap = assetReferenceSearch.search();
        return assetMap.keySet();
    }

    private void replicateContent(Session session, String path) {
        try {
            if(filter(session, path)) {
                log.info("Push aborted.");
                return;
            }
            ReplicationOptions opts = new ReplicationOptions();
            String[] agents = new String[1];
            agents[0] = "push";
            AgentIdFilter agentFilter = new AgentIdFilter(agents);
            opts.setFilter(agentFilter);
            replicator.replicate(session, ReplicationActionType.ACTIVATE, path, opts);
            log.info("Pushed: {}", path);
        } catch (ReplicationException | RepositoryException e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

    private Boolean filter(Session session, String path) throws RepositoryException {
        Node node = session.getNode(path);
        String nodeType = node.getProperty("jcr:primaryType").getString();
        LinkedList<String> nodeTypesToFilter = new LinkedList<String>();
        nodeTypesToFilter.add(JcrResourceConstants.NT_SLING_FOLDER);
        nodeTypesToFilter.add(JcrConstants.NT_FOLDER);
        if(path.contains("/dam/")) {
            return false;
        }
        else if(nodeTypesToFilter.contains(nodeType)) {
            return true;
        }
        else {
            return false;
        }
    }
}
