package com.qualifiedigital.commons.core.workflows;

import com.day.cq.commons.Externalizer;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.osgi.framework.Constants;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;

//Sling Imports
import org.apache.sling.api.resource.ResourceResolverFactory ;
import org.apache.sling.api.resource.ResourceResolver;

import java.util.ArrayList;
import java.util.List;

@Component(property = { Constants.SERVICE_DESCRIPTION + "=Push content to Production-Author instance",
        Constants.SERVICE_VENDOR + "=Qualified Digital",
        "process.label" + "=Push content to Production-Author instance" })
public class PushToProd implements WorkflowProcess {
    private static final Logger log = LoggerFactory.getLogger(PushToProd.class);
    private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36";
    private static final String URL = "/bin/pushtoprod";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private Externalizer externalizer;

    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap processArguments)
            throws WorkflowException {
            try {
                log.info("Starting workflow process to push to prod");
                ResourceResolver resolver = resolverFactory.getThreadResourceResolver();
                String contentPath = workItem.getContentPath();
                String externalizedUrl = externalizer.authorLink(resolver, URL);

                HttpClient client = new HttpClient();
                HttpPost post = new HttpPost(externalizedUrl);

                // add header
                post.setHeader("User-Agent", USER_AGENT);

                List<BasicNameValuePair> urlParameters = new ArrayList<>();
                urlParameters.add(new BasicNameValuePair("_charset_", "utf-8"));
                urlParameters.add(new BasicNameValuePair("cmd", "Activate"));
                urlParameters.add(new BasicNameValuePair("path", contentPath));

                post.setEntity(new UrlEncodedFormEntity(urlParameters));

                HttpMethod method = new PostMethod(URL);

                int response = client.executeMethod(method);
                log.info("\nSending 'POST' request to URL : " + URL);
                log.info("Post parameters : " + post.getEntity());
                log.info("Response Code : " + response);
            }
            catch(Exception e) {

            }
        }
    }