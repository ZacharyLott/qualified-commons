(function(window, document, $, Granite) {
    "use strict";

    var REPLICATE_URL = Granite.HTTP.externalize("/bin/pushtoprod");
    var QUICKPUBLISH_TITLE = Granite.I18n.get("Push To Production");
    var PUBLISH_TEXT = Granite.I18n.get("Push");
    var CANCEL_TEXT = Granite.I18n.get("Cancel");
    var ERROR_TEXT = Granite.I18n.get("Error");

    function activatePagesAndItsReferences(config, collection, selections) {
        var ui = $(window).adaptTo("foundation-ui");
        ui.wait();

        // get pages
        var paths = selections.map(function(v) {
            var item = $(v);

            // allow to set user defined reference paths, split by comma
            var refPath = item.data("checkReferencesPath");
            if (!refPath) {
                // coral3 selection is acting on the masonry level
                refPath = item.children().data("checkReferencesPath");
            }

            if (refPath) {
                return refPath.split(",");
            }

            return item.data("foundationCollectionItemId");
        });

        if (!paths.length) return;

        // flatten the paths
        paths = removeDuplicatesInArray(paths);

        // get the references
        var referencePromise = $.ajax({
            url: Granite.URITemplate.expand(config.data.referenceSrc, {
                path: paths
            }),
            "type": "POST",
            cache: false,
            dataType: "json"
        }).fail(function(xhr) {
            var message = Granite.I18n.getVar($(xhr.responseText).find("#Message").html());
            ui.alert(ERROR_TEXT, message, "error");
        });

        // merge pages and references paths
        referencePromise.done(function(json) {
            // merge all paths
            if (json.assets.length) {
                for (var i = 0; i < json.assets.length; i++ ){
                    var reference = json.assets[i];
                    paths.push(reference.path);
                }
                // flatten the paths
                paths = removeDuplicatesInArray(paths);
            }

            // publish the page and its references directly
            $.ajax({
                url: REPLICATE_URL,
                type: "POST",
                async: false,
                data: {
                    _charset_: "utf-8",
                    cmd: "Activate",
                    path: paths
                }
            }).always(function() {
                ui.clearWait();
            }).done(function() {
                var api = $(collection).adaptTo("foundation-collection");

                if (api && "reload" in api) {
                    api.reload();
                    ui.notify(null, getSuccessMessage(selections));
                    return;
                }

                var contentApi = $(".foundation-content").adaptTo("foundation-content");
                if (contentApi) {
                    contentApi.refresh();
                }

                ui.notify(null, getSuccessMessage(selections));
            }).fail(function(xhr) {
                var title = Granite.I18n.get("Error");
                var message = Granite.I18n.getVar($(xhr.responseText).find("#Message").html());
                ui.alert(title, message, "error");
            });
        });
    }

    function createEl(name) {
        return $(document.createElement(name));
    }

    function getSuccessMessage(selections) {
        var successMessage = Granite.I18n.get("The {0} pages and their references have been published", selections.length);
        if (selections.length === 1) {
            successMessage = Granite.I18n.get("The page and their references have been published");
        }
        return successMessage;
    }

    function removeDuplicatesInArray(anyArray) {
        return anyArray.sort().filter(function(item, pos, ary) {
            return !pos || item != ary[pos - 1];
        });
    }

    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.pushtoprod",
        handler: function(name, el, config, collection, selections) {
            var message = createEl("div");

            var intro = createEl("p").appendTo(message);
            if (selections.length === 1) {
                intro.text(Granite.I18n.get("The page and their references will be pushed."));
            } else {
                intro.text(Granite.I18n.get("The {0} pages and their references will be pushed.", selections.length));
            }

            var ui = $(window).adaptTo("foundation-ui");
            ui.prompt(QUICKPUBLISH_TITLE, message.html(), "notice", [{
                text: CANCEL_TEXT
            }, {
                text: PUBLISH_TEXT,
                primary: true,
                handler: function() {
                    activatePagesAndItsReferences(config, collection, selections);
                }
            }]);
        }
    });
})(window, document, Granite.$, Granite);
